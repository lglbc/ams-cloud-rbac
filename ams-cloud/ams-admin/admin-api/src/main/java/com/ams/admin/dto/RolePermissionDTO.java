package com.ams.admin.dto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
@Accessors(chain = true)
public class RolePermissionDTO {
    private Long roleId;
    private List<Long> permissionIds;
    private Long menuId;
}
