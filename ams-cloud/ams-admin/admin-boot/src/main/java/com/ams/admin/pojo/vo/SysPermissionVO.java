package com.ams.admin.pojo.vo;

import com.ams.admin.pojo.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
@Accessors(chain = true)
public class SysPermissionVO implements Serializable {

    private Long id;

    private String name;

    private Long menuId;

    private String urlPerm;

    private String btnSign;

}
