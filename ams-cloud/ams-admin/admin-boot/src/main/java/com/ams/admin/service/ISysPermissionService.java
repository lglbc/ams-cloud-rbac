package com.ams.admin.service;

import com.ams.admin.pojo.entity.SysPermission;
import com.ams.admin.pojo.req.CommonReq;
import com.ams.admin.pojo.req.SavePermissionReq;
import com.ams.admin.pojo.vo.SysPermissionVO;
import com.ams.admin.pojo.vo.SysServiceVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface ISysPermissionService extends IService<SysPermission> {
    /**
     * 刷新Redis缓存中角色菜单的权限规则，角色和菜单信息变更调用
     */
    boolean refreshPermRolesRules();

    List<SysPermission> listPermRoles();

    /**
     * 获取权限列表
     * @param menuId
     * @return
     */
    List<SysPermissionVO> listByMenuId(Long menuId);

    /**
     * 新增权限
     * @param req
     */
    void createPermission(SavePermissionReq req);

    /**
     *
     * 更新权限
     * @param req
     */
    void updatePermission(SavePermissionReq req);

    /**
     * 获取所有服务列表
     * @return
     */
    List<SysServiceVO> getServices();

    /**
     * 删除权限
     * @param ids
     */
    void deletePermissions(List<Long> ids);

    /**
     * 查询角色绑定的权限
     * @param roleId
     * @return
     */
    List<Long> listRolePermission(Long roleId);

    /**
     * 更新角色绑定的权限信息
     * @param roleId
     * @param req
     */
    void updateRolePermission(Long roleId, CommonReq req);
}
