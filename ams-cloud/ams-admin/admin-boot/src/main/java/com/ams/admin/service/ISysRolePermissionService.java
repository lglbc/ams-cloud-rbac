package com.ams.admin.service;


import com.ams.admin.pojo.entity.SysRoleMenu;
import com.ams.admin.pojo.entity.SysRolePermission;
import com.ams.admin.pojo.req.CommonReq;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

}
