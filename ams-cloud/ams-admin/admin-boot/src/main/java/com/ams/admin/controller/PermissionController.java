package com.ams.admin.controller;

import com.ams.admin.pojo.req.CommonReq;
import com.ams.admin.pojo.req.SavePermissionReq;
import com.ams.admin.pojo.vo.SysPermissionVO;
import com.ams.admin.pojo.vo.SysRoleSelectVO;
import com.ams.admin.pojo.vo.SysServiceVO;
import com.ams.admin.service.ISysPermissionService;
import com.ams.admin.service.ISysRoleService;
import com.ams.common.result.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@RestController
@RequestMapping("/permission")
@Slf4j
@RequiredArgsConstructor
public class PermissionController {
    private final ISysPermissionService permissionService;

    /**
     * 权限列表
     */
    @GetMapping("/list/{menuId}")
    public R<List<SysPermissionVO>> list(@PathVariable Long menuId){
        List<SysPermissionVO> permissionVOS = permissionService.listByMenuId(menuId);
        return R.ok(permissionVOS);
    }

    /**
     * 创建权限
     */
    @PostMapping
    public R createPermission(@Validated @RequestBody SavePermissionReq req){
        permissionService.createPermission(req);
        return R.ok();
    }

    /**
     * 更新权限
     */
    @PutMapping
    public R updatePermission(@Validated @RequestBody SavePermissionReq req){
        permissionService.updatePermission(req);
        return R.ok();
    }

    /**
     * 获取服务列表
     */
    @GetMapping("/services")
    public R<List<SysServiceVO>> getServices(){
        List<SysServiceVO> sysServiceVOS = permissionService.getServices();
        return R.ok(sysServiceVOS);
    }
    /**
     * 删除权限
     */
    @DeleteMapping("/{ids}")
    public R getServices(@PathVariable List<Long> ids){
        permissionService.deletePermissions(ids);
        return R.ok();
    }
    /**
     * 查询角色绑定的权限
     */
    @GetMapping("/role/{roleId}")
    public R<List<Long>> listRolePermission(@PathVariable Long roleId){
        List<Long> permissions = permissionService.listRolePermission(roleId);
        return R.ok(permissions);
    }

    /**
     * 更新角色绑定的权限
     */
    @PutMapping("/role/{roleId}")
    public R updateRolePermission(@PathVariable Long roleId, @RequestBody CommonReq req) {
        permissionService.updateRolePermission(roleId,req);
        return R.ok();
    }
}
