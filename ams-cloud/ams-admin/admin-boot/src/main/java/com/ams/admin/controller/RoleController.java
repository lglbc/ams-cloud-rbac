package com.ams.admin.controller;

import com.ams.admin.pojo.req.RoleListPageReq;
import com.ams.admin.pojo.req.SaveSysRoleReq;
import com.ams.admin.pojo.vo.SysRoleSelectVO;
import com.ams.admin.pojo.vo.SysRoleVO;
import com.ams.admin.service.ISysRoleService;
import com.ams.common.entity.APage;
import com.ams.common.result.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@RestController
@RequestMapping("/role")
@Slf4j
@RequiredArgsConstructor
public class RoleController {
    private final ISysRoleService roleService;

    /**
     * 角色列表select
     */
    @GetMapping("/select")
    public R<List<SysRoleSelectVO>> roleSelect() {
        List<SysRoleSelectVO> roleVOList = roleService.roleSelect();
        return R.ok(roleVOList);
    }

    /**
     * 列表分页
     */
    @GetMapping("/listPage")
    public R<List<SysRoleVO>> listPage(RoleListPageReq req) {
        APage<SysRoleVO> roleVOAPage = roleService.listPage(req);
        return R.page(roleVOAPage);
    }

    /**
     * 创建角色
     */
    @PostMapping
    public R createRole(@Validated @RequestBody SaveSysRoleReq req) {
        roleService.createRole(req);
        return R.ok();
    }

    /**
     * 更新角色
     */
    @PutMapping
    public R updateRole(@Validated @RequestBody SaveSysRoleReq req) {
        roleService.updateRole(req);
        return R.ok();
    }

    /**
     * 更新角色状态
     */
    @PatchMapping("/updateStatus/{id}/{status}")
    public R updateRole(@PathVariable Long id,@PathVariable int status) {
        roleService.updateStatus(id,status);
        return R.ok();
    }

    /**
     * 删除角色
     */
    @DeleteMapping("/{ids}")
    public R deletes(@PathVariable List<Long> ids) {
        roleService.deletes(ids);
        return R.ok();
    }


}
