package com.ams.admin.service;


import com.ams.admin.pojo.entity.SysMenu;
import com.ams.admin.pojo.entity.SysRole;
import com.ams.admin.pojo.req.CommonReq;
import com.ams.admin.pojo.req.SaveMenuReq;
import com.ams.admin.pojo.vo.SysMenuSelectVO;
import com.ams.admin.pojo.vo.SysMenuVO;
import com.ams.admin.pojo.vo.SysRoleSelectVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface ISysMenuService extends IService<SysMenu> {
    /**
     * 加载数据表格菜单
     * @param name
     */
    List<SysMenuVO> loadMenus();

    /**
     * 创建菜单
     * @param req
     */
    void createMenu(SaveMenuReq req);

    /**
     * 菜单树
     * @return
     */
    List<SysMenuVO> listTree();

    /**
     * 更新菜单
     * @param req
     */
    void updateMenu(SaveMenuReq req);

    /**
     * 批量删除
     * @param ids
     */
    void deletes(List<Long> ids);

    /**
     * 获取菜单select
     * @return
     */
    List<SysMenuSelectVO> select(int status);

    /**
     * 获取菜单详情
     * @param id
     * @return
     */
    SysMenuVO getDetail(Long id);

    /**
     * 查询角色绑定的菜单
     * @param roleId
     * @return
     */
    List<Long> listRoleMenu(Long roleId);

    /**
     * 更新角色绑定的菜单
     * @param roleId
     * @param req
     */
    void updateRoleMenu(Long roleId, CommonReq req);

    void updateMenuStatus(Long id, int status);

    List<Long> currentUser();
}
