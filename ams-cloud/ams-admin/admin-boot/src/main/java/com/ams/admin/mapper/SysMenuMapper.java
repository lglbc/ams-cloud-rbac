package com.ams.admin.mapper;

import com.ams.admin.pojo.entity.SysMenu;
import com.ams.admin.pojo.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
