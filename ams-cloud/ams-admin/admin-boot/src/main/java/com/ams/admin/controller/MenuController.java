package com.ams.admin.controller;

import com.ams.admin.pojo.req.CommonReq;
import com.ams.admin.pojo.req.SaveMenuReq;
import com.ams.admin.pojo.vo.SysMenuSelectVO;
import com.ams.admin.pojo.vo.SysMenuVO;
import com.ams.admin.pojo.vo.SysRoleMenuVO;
import com.ams.admin.service.ISysMenuService;
import com.ams.common.result.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@RestController
@RequestMapping("/menu")
@Slf4j
@RequiredArgsConstructor
public class MenuController {
    private final ISysMenuService sysMenuService;

    /**
     * 菜单详情
     */
    @GetMapping("/{id}")
    public R<SysMenuVO> getDetail(@PathVariable Long id) {
        SysMenuVO sysMenuVO = sysMenuService.getDetail(id);
        return R.ok(sysMenuVO);
    }

    /**
     * 获取菜单数据列表
     *
     * @return
     */
    @GetMapping("/list")
    public R<List<SysMenuVO>> list() {
        List<SysMenuVO> sysMenuVOS = sysMenuService.loadMenus();
        return R.ok(sysMenuVOS);
    }

    /**
     * 创建菜单
     */
    @PostMapping
    public R createMenu(@Validated @RequestBody SaveMenuReq req) {
        sysMenuService.createMenu(req);
        return R.ok();
    }

    /**
     * 菜单树
     */
    @GetMapping("/listTree")
    public R<List<SysMenuVO>> listTree() {
        List<SysMenuVO> menuVOS = sysMenuService.listTree();
        return R.ok(menuVOS);
    }

    /**
     * 更新菜单
     */
    @PutMapping
    public R updateMenu(@Validated @RequestBody SaveMenuReq req) {
        sysMenuService.updateMenu(req);
        return R.ok();
    }
    /**
     * 更新菜单
     */
    @PatchMapping("/{id}/{status}")
    public R updateMenuStatus(@PathVariable Long id,@PathVariable int status) {
        sysMenuService.updateMenuStatus(id,status);
        return R.ok();
    }

    /**
     * 删除菜单
     */
    @DeleteMapping("/{ids}")
    public R deletes(@PathVariable List<Long> ids) {
        sysMenuService.deletes(ids);
        return R.ok();
    }

    /**
     * 菜单select
     */

    @GetMapping("/select/{status}")
    public R<List<SysMenuSelectVO>> select(@PathVariable int status) {
        List<SysMenuSelectVO> selectVOS = sysMenuService.select(status);
        return R.ok(selectVOS);
    }

    /**
     * 查询角色绑定的菜单
     */
    @GetMapping("/role/{roleId}")
    public R<List<Long>> listRoleMenu(@PathVariable Long roleId) {
        List<Long> menuIds = sysMenuService.listRoleMenu(roleId);
        return R.ok(menuIds);
    }


    /**
     * 更新角色绑定的菜单
     */
    @PutMapping("/role/{roleId}")
    public R updateRoleMenu(@PathVariable Long roleId,@RequestBody CommonReq req) {
        sysMenuService.updateRoleMenu(roleId,req);
        return R.ok();
    }

    /**
     * 获取当前用户绑定的菜单
     */

    @GetMapping("/currentUser")
    public R<List<Long>> currentUser(){
        List<Long> menus = sysMenuService.currentUser();
        return R.ok(menus);

    }
}
