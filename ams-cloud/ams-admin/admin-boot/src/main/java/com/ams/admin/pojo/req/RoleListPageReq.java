package com.ams.admin.pojo.req;

import com.ams.common.entity.PageReq;
import lombok.Data;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class RoleListPageReq extends PageReq {
    private String keyword;
}
