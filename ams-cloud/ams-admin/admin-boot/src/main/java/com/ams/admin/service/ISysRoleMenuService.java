package com.ams.admin.service;


import com.ams.admin.pojo.entity.SysRole;
import com.ams.admin.pojo.entity.SysRoleMenu;
import com.ams.admin.pojo.req.RoleListPageReq;
import com.ams.admin.pojo.req.SaveSysRoleReq;
import com.ams.admin.pojo.vo.SysRoleSelectVO;
import com.ams.admin.pojo.vo.SysRoleVO;
import com.ams.common.entity.APage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {


}
