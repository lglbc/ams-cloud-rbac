package com.ams.admin.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class SysRoleSelectVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
    * id
    */
    private Long id;

    /**
    * 角色名称
    */
    private String name;
}