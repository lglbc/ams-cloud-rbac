package com.ams.admin.service;


import com.ams.admin.pojo.entity.SysRole;
import com.ams.admin.pojo.req.RoleListPageReq;
import com.ams.admin.pojo.req.SaveSysRoleReq;
import com.ams.admin.pojo.vo.SysRoleSelectVO;
import com.ams.admin.pojo.vo.SysRoleVO;
import com.ams.common.entity.APage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 角色列表select
     *
     * @return
     */
    List<SysRoleSelectVO> roleSelect();

    /**
     * 列表分页
     * @param req
     * @return
     */
    APage<SysRoleVO> listPage(RoleListPageReq req);

    /**
     * 创建角色
     * @param req
     */
    void createRole(SaveSysRoleReq req);

    /**
     * 更新角色
     * @param req
     */
    void updateRole(SaveSysRoleReq req);

    /**
     * 删除角色
     */
    void deletes(List<Long> ids);


    /**
     * 更新角色状态
     * @param id
     * @param status
     */
    void updateStatus(Long id, int status);
}
