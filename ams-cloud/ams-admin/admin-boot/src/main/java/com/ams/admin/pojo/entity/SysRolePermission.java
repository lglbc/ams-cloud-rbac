package com.ams.admin.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRolePermission {
    private Long permissionId;
    private Long roleId;
}
