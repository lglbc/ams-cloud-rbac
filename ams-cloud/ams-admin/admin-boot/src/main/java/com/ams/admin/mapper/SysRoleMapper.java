package com.ams.admin.mapper;

import com.ams.admin.dto.UserAuthDTO;
import com.ams.admin.pojo.entity.SysRole;
import com.ams.admin.pojo.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
