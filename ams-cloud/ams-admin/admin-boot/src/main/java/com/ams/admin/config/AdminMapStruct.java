package com.ams.admin.config;

import com.ams.admin.pojo.entity.SysPermission;
import com.ams.admin.pojo.entity.SysRole;
import com.ams.admin.pojo.entity.SysUser;
import com.ams.admin.pojo.vo.SysPermissionVO;
import com.ams.admin.pojo.vo.SysRoleSelectVO;
import com.ams.admin.pojo.vo.SysRoleVO;
import com.ams.admin.pojo.vo.SysUserVO;
import org.mapstruct.Mapper;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Mapper(componentModel = "spring")
public interface AdminMapStruct {
    List<SysRoleSelectVO> sysRoleToSysRoleVO(List<SysRole> sysRoles);
    List<SysUserVO> sysUserToSysUserVO(List<SysUser> sysUsers);
    List<SysRoleVO> sysRoleToSysRoleListVO(List<SysRole> sysRoles);
    List<SysPermissionVO> sysPermissionToSysPermissionVO(List<SysPermission> permissions);
}
