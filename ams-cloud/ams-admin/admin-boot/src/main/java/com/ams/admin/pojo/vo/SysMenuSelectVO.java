package com.ams.admin.pojo.vo;

import lombok.Data;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class SysMenuSelectVO {
    private Long id;
    private String label;
    private List<SysMenuSelectVO> children;

}
