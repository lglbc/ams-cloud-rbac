package com.ams.admin.service;

import com.ams.admin.pojo.entity.SysOauthClient;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface ISysOauthClientService extends IService<SysOauthClient> {
}
