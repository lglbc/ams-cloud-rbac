package com.ams.admin.pojo.req;

import lombok.Data;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class CommonReq {
    private List<Long> ids;
}
