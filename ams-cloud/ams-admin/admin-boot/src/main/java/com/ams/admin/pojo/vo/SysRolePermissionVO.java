package com.ams.admin.pojo.vo;

import lombok.Data;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class SysRolePermissionVO {
    private Long permissionId;
    private Long role_id;
}
