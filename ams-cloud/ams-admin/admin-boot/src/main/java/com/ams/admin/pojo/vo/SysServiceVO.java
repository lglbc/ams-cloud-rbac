package com.ams.admin.pojo.vo;

import lombok.Data;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class SysServiceVO {
    private String serviceCode;
    private String serviceName;
}
