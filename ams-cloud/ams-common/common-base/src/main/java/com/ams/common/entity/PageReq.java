package com.ams.common.entity;

import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class PageReq implements Serializable {
    private Long pageNo;
    private Long pageSize;
}
