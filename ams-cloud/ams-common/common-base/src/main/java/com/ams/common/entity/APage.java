package com.ams.common.entity;

import lombok.Data;

import java.util.List;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class APage<T> {
    private Long total;
    private Long pageNo;
    private Long pageSize;
    private List<T> list;
}
