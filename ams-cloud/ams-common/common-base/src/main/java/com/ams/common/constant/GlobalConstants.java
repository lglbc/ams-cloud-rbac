package com.ams.common.constant;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
public interface GlobalConstants {

    String URL_PERM_ROLES_KEY = "system:perm_roles_rule:url:";
    Integer STATUS_ON = 1;
    Integer STATUS_OFF = 0;
    String USER_DEFAULT_PASSWORD = "123456";
    Long ROOT_MENU_ID = -1L;
    String ADMIN_URL_PERM = "%s:/%s%s";
}
