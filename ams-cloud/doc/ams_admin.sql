/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.64.2
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 192.168.64.2:3306
 Source Schema         : ams_admin

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 23/04/2022 15:42:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
                            `id` bigint NOT NULL AUTO_INCREMENT,
                            `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单名称',
                            `parent_id` bigint DEFAULT NULL COMMENT '父菜单ID',
                            `path` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '路由路径',
                            `component` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '组件路径',
                            `icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单图标',
                            `sort` int DEFAULT '0' COMMENT '排序',
                            `visible` tinyint(1) DEFAULT '1' COMMENT '状态：0-禁用 1-开启',
                            `redirect` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '跳转路径',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `create_by` bigint DEFAULT NULL,
                            `update_by` bigint DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=utf8mb3 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` (`id`, `name`, `parent_id`, `path`, `component`, `icon`, `sort`, `visible`, `redirect`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (1, '系统管理', -1, '/admin', 'Layout', ' el-icon-setting', 1, 1, NULL, '2021-08-28 09:12:21', '2022-04-23 12:33:50', NULL, 4);
INSERT INTO `sys_menu` (`id`, `name`, `parent_id`, `path`, `component`, `icon`, `sort`, `visible`, `redirect`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (2, '用户管理', 1, 'user', '/admin/user/index', 'el-icon-user', 1, 1, NULL, '2021-08-29 09:12:21', '2021-08-29 09:12:21', NULL, NULL);
INSERT INTO `sys_menu` (`id`, `name`, `parent_id`, `path`, `component`, `icon`, `sort`, `visible`, `redirect`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (3, '菜单管理', 1, 'menu', '/admin/menu/index', 'el-icon-menu', 2, 1, NULL, '2021-08-28 09:12:21', '2021-08-28 09:12:21', NULL, NULL);
INSERT INTO `sys_menu` (`id`, `name`, `parent_id`, `path`, `component`, `icon`, `sort`, `visible`, `redirect`, `create_time`, `update_time`, `create_by`, `update_by`) VALUES (234, '角色管理', 1, '/role', '/admin/role/index', 'el-icon-s-custom', 2, 1, '', '2022-04-23 12:36:11', '2022-04-23 12:36:11', 4, 4);
COMMIT;

-- ----------------------------
-- Table structure for sys_oauth_client
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_client`;
CREATE TABLE `sys_oauth_client` (
                                    `client_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                                    `resource_ids` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `client_secret` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `scope` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `authorized_grant_types` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `web_server_redirect_uri` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `authorities` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `access_token_validity` int DEFAULT NULL,
                                    `refresh_token_validity` int DEFAULT NULL,
                                    `additional_information` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `autoapprove` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
                                    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                                    `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新者',
                                    `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建者',
                                    PRIMARY KEY (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of sys_oauth_client
-- ----------------------------
BEGIN;
INSERT INTO `sys_oauth_client` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES ('ams', NULL, 'ams', 'all', 'authorization_code,password,refresh_token,implicit,wechat,refresh_token', 'http://www.baidu.com', NULL, 360000000, 720000, NULL, 'true', NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
                                  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '权限名称',
                                  `menu_id` int DEFAULT NULL COMMENT '菜单模块ID\r\n',
                                  `url_perm` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'URL权限标识',
                                  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                                  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新者',
                                  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建者',
                                  `btn_sign` varchar(255) DEFAULT NULL COMMENT '按钮标识',
                                  PRIMARY KEY (`id`) USING BTREE,
                                  KEY `id` (`id`,`name`) USING BTREE,
                                  KEY `id_2` (`id`,`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COMMENT='权限表';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_permission` (`id`, `name`, `menu_id`, `url_perm`, `create_time`, `update_time`, `update_by`, `create_by`, `btn_sign`) VALUES (13, '新增', 2, 'POST:/ams-admin/user', '2022-04-23 14:07:02', '2022-04-23 14:07:02', '4', '4', 'admin:user:add');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
                            `id` bigint NOT NULL AUTO_INCREMENT,
                            `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
                            `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色编码',
                            `sort` int DEFAULT NULL COMMENT '显示顺序',
                            `status` tinyint(1) DEFAULT '1' COMMENT '角色状态：0-正常；1-停用',
                            `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除标识：0-未删除；1-已删除',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新者',
                            `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建者',
                            PRIMARY KEY (`id`) USING BTREE,
                            UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`id`, `name`, `code`, `sort`, `status`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (4, '管理员', 'admin', 1, 1, 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_role` (`id`, `name`, `code`, `sort`, `status`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (5, '组管理员', 'root', 2, 0, 0, NULL, '2022-04-16 05:39:33', '4', NULL);
INSERT INTO `sys_role` (`id`, `name`, `code`, `sort`, `status`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (6, '角色名称2222', '角色编2222', 11, 0, 1, '2022-04-14 05:50:55', '2022-04-14 05:51:48', '32', '32');
INSERT INTO `sys_role` (`id`, `name`, `code`, `sort`, `status`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (7, '角色名称', '角色编码', 1, 1, 0, '2022-04-14 05:55:51', '2022-04-14 05:55:51', '32', '32');
INSERT INTO `sys_role` (`id`, `name`, `code`, `sort`, `status`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (8, '部门管理员', 'DEPT_ADMIN', 12123, 1, 0, '2022-04-16 05:53:25', '2022-04-16 05:53:25', '4', '4');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
                                 `role_id` bigint NOT NULL COMMENT '角色ID',
                                 `menu_id` bigint NOT NULL COMMENT '菜单ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (4, 1);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (4, 2);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (4, 3);
INSERT INTO `sys_role_menu` (`role_id`, `menu_id`) VALUES (4, 234);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
                                       `role_id` int DEFAULT NULL COMMENT '角色id',
                                       `permission_id` int DEFAULT NULL COMMENT '资源id',
                                       KEY `role_id` (`role_id`) USING BTREE,
                                       KEY `permission_id` (`permission_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='角色权限表';

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_permission` (`role_id`, `permission_id`) VALUES (4, 13);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名',
                            `nickname` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '昵称',
                            `gender` tinyint(1) DEFAULT '1' COMMENT '性别：1-男 2-女',
                            `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
                            `avatar` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '用户头像',
                            `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系方式',
                            `status` tinyint(1) DEFAULT '1' COMMENT '用户状态：1-正常 0-禁用',
                            `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户邮箱',
                            `deleted` tinyint(1) DEFAULT '0' COMMENT '逻辑删除标识：0-未删除；1-已删除',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新者',
                            `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建者',
                            PRIMARY KEY (`id`) USING BTREE,
                            UNIQUE KEY `login_name` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb3 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (4, 'ams', '管理员', 1, '$2a$10$cmyqsFYXMp0H5d44LmHDbuEpJMeDJqFkKHRvo61g7H8KcORGX0tcG', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg3.53shop.com%2FUploadFile%2Fnewspic%2F2018%2F05%2F17%2F20180517103618371.gif&refer=http%3A%2F%2Fimg3.53shop.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1651571208&t=bb7ea42de6eb55ad0d7a64e3d42f6afc', '', 1, '', 0, '2021-12-04 20:14:09', '2021-12-04 20:14:12', NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (9, 'aims', 'AI码师 更新2222', 1, '$2a$10$f8NWQMlPeobL6c8TfxZHLOcd360MVehxUpGNKUIR5m/2OpwAmlexe', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, '9', NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (10, 'aims1', 'AI码师1', 1, '$2a$10$0MuUDE/.yjvmHjiPapubouBFt0S/QfrZZ559Y4B9BkDH81g7aNxTu', '', '13326589562', 0, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (11, 'aims2', 'AI码师2', 1, '$2a$10$ynt73opbWYFlFdjB0c2rW.uvzdenbgRVaepF68XGNuLessgdw7ufa', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (12, 'aims3', 'AI码师3', 1, '$2a$10$jMLYCbdei1hFVWrYG.J.DOZFc35V1DrjlgaT.Nlb7grT5qB4T637.', '', '13326589562', 1, '13326589562@qq.com', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (13, 'aims4', 'AI码师4', 1, '$2a$10$bTwRqr36bfUftj9uZmU1y.FvbBZfPYNjYaC6ied0p/4BFe/kCdZE.', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (14, 'aims5', 'AI码师4', 1, '$2a$10$vf492.HYyGiA.uUtp6kDXOL9e5ck81chKId9qKylo3zbXTMOKQ1B.', '', '13326589562', 1, '13326589562@qq.com', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (16, 'aims6', 'AI码师5', 1, '$2a$10$DdjY8bqjY7zy35cbHC6OS.3FJNssQTGRty75xlPK0dlQ00NL2wY/6', '', '13326589562', 1, '13326589562@qq.com', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (17, 'aims7', 'AI码师7', 1, '$2a$10$wO2hcLQQl3aTiIQAqvhb2eNwgcWvdqmySM8KUgbhtW7M1vcGrZKhy', '', '13326589562', 1, '13326589562@qq.com', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (18, 'aims8', 'AI码师8', 1, '$2a$10$BIiSiqoWbxE..dIPsjPFTOxMa7EKcSSCZyVXhBphQtVlakiISAJsy', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (19, 'aims9', 'AI码师9', 1, '$2a$10$NUmtcws6.rjsrJZVb1qlPOn4PD9BzgP79uzracHjY3RAwDL6bQ84i', '', '13326589562', 1, '13326589562@qq.com', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (20, 'aims10', 'AI码师10', 1, '$2a$10$3BuvqXlahKiS0y0Bqoee6OX/Orc5x9t/xfhdHwOACpodXtZZ7zg2.', '', '13326589562', 1, '13326589562@qq.com', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (21, 'aims11', 'AI码师10', 1, '$2a$10$RwBQLnurfrIIOmCRmFU3redu0JKd7KV.qe2WsQzKJal.3k5FCdwm.', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (22, 'aims12', 'AI码师10', 1, '$2a$10$EhmzF45fsJ8oHQENXV9cR.oa39cVLKMxYAqWAJTiduYMtNkFWxAnu', '', '13326589562', 1, '13326589562@qq.com', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (23, 'aims13', 'AI码师10', 1, '$2a$10$HBeyURYlmI4RmAMROzqpAe6tMkgH0PjHhh95ykwZkeTjellnThZpq', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (24, 'aims14', 'AI码师10', 1, '$2a$10$zTg2geUOgKIj4363X1T1Z..gzEk5f8acEfoWTZQ53JmFM6ph76aIe', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (25, 'aims15', 'AI码师10', 1, '$2a$10$tg6J9mz8CxBzz.SwFpksJu401Mr5CmjPlJ610jo86czA8zRCPMjmy', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (26, 'student', '学生', 0, '$2a$10$yuB1rbbAxIaKCWR61I24OOsy89vdbskiMOWnH2SmfTQHoXg1U/qNG', '', '13095685896', 0, '2666@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (27, 'aims2222', 'AI码师', 1, '$2a$10$6ewmCqtLzNz91E7dJ.Fry.fdtaxQ6AASu.50SE7mw0Rw0ejJEZKly', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (28, 'aims2223', 'AI码师', 1, '$2a$10$YkN48wgWAvU0QsOss4wokelsAIh1zRmJsaptV460fLUFWwXVvXIrO', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (29, 'aims22234', 'AI码师', 1, '$2a$10$/i7YWwfAyEqg.dCgdRneduB3QHb0Gj3uK8TD67Igafc.R335s5TxC', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (30, 'aims222347', 'AI码师', 1, '$2a$10$iN584fIWfMXEAYT6F/1Qtu45CuRDWnK1E5LHsYaM/K1hOtWCcBd4a', '', '13326589562', 1, '13326589562@qq.com', 0, '2022-04-04 09:37:37', '2022-04-04 09:37:37', NULL, NULL);
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (31, '20220404', 'AI码师', 1, '$2a$10$K3lcHcoGqh/iv/I1ZzHNYO8ZLJOr3.BvamtU17nfGUuMJPPMYFs/i', '', '13326589562', 1, '13326589562@qq.com', 0, NULL, NULL, '9', '9');
INSERT INTO `sys_user` (`id`, `username`, `nickname`, `gender`, `password`, `avatar`, `mobile`, `status`, `email`, `deleted`, `create_time`, `update_time`, `update_by`, `create_by`) VALUES (32, '20220404001', 'AI码师 更新2222 自动注入', 1, '$2a$10$QkWDX0SloqCZpZa21j.hY.Gw7zY8TLMNGYAM2fABKEGt0TPaSIKIG', '', '13326589562', 1, '13326589562@qq.com', 0, '2022-04-04 11:15:38', '2022-04-04 11:17:19', '32', '9');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
                                 `user_id` int NOT NULL COMMENT '用户ID',
                                 `role_id` int NOT NULL COMMENT '角色ID',
                                 PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (4, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (4, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (9, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (9, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (10, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (10, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (11, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (11, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (13, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (13, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (18, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (18, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (21, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (21, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (23, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (23, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (24, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (24, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (26, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (27, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (27, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (28, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (28, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (29, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (29, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (30, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (30, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (31, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (31, 5);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (32, 4);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (32, 5);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
