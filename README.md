# 乐哥聊编程 /  AI码师微服务权限管理
每年分享100+个技术视频，做知识的传播者

SpringCloud Alibaba + Vue +ElementUI

# 知识库
[知识库导航](https://lglbc.gitee.io/)

# 配套文档
关注公众号“乐哥聊编程”，通过对应菜单栏找到对应项目文档
# 配套视频：
* [西瓜视频](https://studio.ixigua.com/content)
* [B站视频](https://space.bilibili.com/483046434/channel/series)
* [知乎视频](https://www.zhihu.com/people/yang-le-25-8/zvideos)

加作者微信：AmsNeil
拉你进限时免费技术交流群，不定期分享各种内卷资料 :bowtie: 欢迎各路人士加入，一起做技术的分享者
