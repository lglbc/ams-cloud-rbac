import request from '@/utils/request'

/**
 * 菜单列表
 * @returns {AxiosPromise}
 */
export function list() {
  return request({
    url: '/ams-admin/menu/list',
    method: 'get'
  })
}

/**
 * 菜单详情
 * @returns {AxiosPromise}
 */
export function menuDetail(id) {
  return request({
    url: `/ams-admin/menu/${id}`,
    method: 'get'
  })
}

/**
 * 菜单select
 * @returns {AxiosPromise}
 */
export function menuSelect(status) {
  return request({
    url: `/ams-admin/menu/select/${status}`,
    method: 'get'
  })
}
/**
 * 菜单树
 * @returns {AxiosPromise}
 */
export function menuRoutes() {
  return request({
    url: `/ams-admin/menu/listTree`,
    method: 'get'
  })
}
/**
 * 菜单树
 * @returns {AxiosPromise}
 */
export function currentUserMenus() {
  return request({
    url: `/ams-admin/menu/currentUser`,
    method: 'get'
  })
}

/**
 * 更新菜单
 * @returns {AxiosPromise}
 */
export function updateMenu(data) {
  return request({
    url: `/ams-admin/menu`,
    method: 'put',
    data
  })
}

/**
 * 更新菜单状态
 * @returns {AxiosPromise}
 */
export function updateMenuStatus(id, status) {
  return request({
    url: `/ams-admin/menu/${id}/${status}`,
    method: 'patch'
  })
}

/**
 * 新增 菜单
 * @returns {AxiosPromise}
 */
export function createMenu(data) {
  return request({
    url: `/ams-admin/menu`,
    method: 'post',
    data
  })
}

/**
 * 删除 菜单
 * @returns {AxiosPromise}
 */
export function deleteMenus(ids) {
  return request({
    url: `/ams-admin/menu/${ids}`,
    method: 'delete'
  })
}

/**
 * 查询角色绑定的菜单
 * @returns {AxiosPromise}
 */
export function roleMenus(roleId) {
  return request({
    url: `/ams-admin/menu/role/${roleId}`,
    method: 'get'
  })
}

/**
 * 保存角色绑定的菜单
 * @returns {AxiosPromise}
 */
export function saveRoleMenus(roleId, data) {
  return request({
    url: `/ams-admin/menu/role/${roleId}`,
    method: 'put',
    data
  })
}

