import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/ams-auth/oauth/token',
    method: 'post',
    params: data,
    headers: {
      'authorization': 'Basic YW1zOmFtcw=='
    }
  })
}

export function getInfo() {
  return request({
    url: '/ams-admin/user/current',
    method: 'get'
  })
}

/**
 * 获取用户分页
 * @param params
 * @returns {AxiosPromise}
 */
export function listPage(params) {
  return request({
    url: '/ams-admin/user/listPage',
    method: 'get',
    params
  })
}

/**
 * 更新用户状态
 * @param userId
 * @param status
 * @returns {AxiosPromise}
 */
export function updateStatus(userId, status) {
  return request({
    url: `/ams-admin/user/updateStatus/${userId}/${status}`,
    method: 'patch'
  })
}

/**
 * 查询用户详情
 * @returns {AxiosPromise}
 */
export function userDetail(userId) {
  return request({
    url: `/ams-admin/user/${userId}`,
    method: 'get'
  })
}
/**
 * 更新用户信息
 * @returns {AxiosPromise}
 */
export function updateUserInfo(userId, data) {
  return request({
    url: `/ams-admin/user/${userId}`,
    method: 'put',
    data: data
  })
}
/**
 * 创建用户信息
 * @returns {AxiosPromise}
 */
export function createUserInfo(data) {
  return request({
    url: `/ams-admin/user`,
    method: 'post',
    data: data
  })
}
/**
 * 更新用户信息
 * @returns {AxiosPromise}
 */
export function deletes(userIds) {
  return request({
    url: `/ams-admin/user/${userIds.join(',')}`,
    method: 'delete'
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

